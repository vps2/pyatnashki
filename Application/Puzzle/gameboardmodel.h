#ifndef GAMEBOARDMODEL_H
#define GAMEBOARDMODEL_H

#include "igameboardmodel.h"

#include <QAbstractTableModel>
#include <QModelIndex>
#include <QVector>

class GameBoardModel : public QAbstractTableModel, public IGameBoardModel
{
      Q_OBJECT

   public:
      explicit GameBoardModel(QObject *parent = nullptr);
      GameBoardModel(int rows, int columns, QObject *parent = nullptr);

      //*** Реализация абстрактных методов QAbstractTableModel ***
      //
      int rowCount(const QModelIndex &index) const override;	//аргументы по умолчанию не "наследуются"
      int columnCount(const QModelIndex &index) const override;
      QVariant data(const QModelIndex &index, int role) const override;
      //
      //***

      //*** Реализация открытых методов интерфейса IGameBoardModel
      //
      bool moveElement(const QModelIndex &currCellIndex) override;
      void shuffle() override;
      bool operator==(const IGameBoardModel &other) const override;
      //
      //***

   private:
      void initialize();
      QModelIndexList getAdjacentIndexes(const QModelIndex &currIndex) const;
      bool setStorageData(const QModelIndex &index, const QVariant &value);

      int m_numberOfRows;
      int m_numberOfColumns;

      QModelIndex m_emptyIndex;
      QVector<QVector<int>> m_storage;
};

#endif // GAMEBOARDMODEL_H
