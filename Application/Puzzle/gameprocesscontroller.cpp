/*!
  \~russian

	\class GameProcessController

	\brief Контроллер игрового процесса.

	Тут должно быть полное описание работы класса...
*/

#include "gameprocesscontroller.h"
#include "igameboardmodel.h"
#include "gametimer.h"

/*!
 * \~russian
 * Конструктор контроллера игрового процесса.
 * @fn GameProcessController::GameProcessController(IGameBoardModel *workModel,
 *																	 IGameBoardModel *refModel,
 *																	 QObject *parent)
 * @param workModel модель игрового поля, используемая в качестве рабочей.
 * @param refModel модель игрового поля, используемая в качестве эталонной.
 * @param parent
 *
 * \warning
 * 1. Параметр workModel не должен быть нулевым
 * 2. Параметр refModel не должен быть нулевым
 * 3. Параметр workModel и refModel должны указывать на различные объекты
 */
GameProcessController::GameProcessController(IGameBoardModel *workModel,
															IGameBoardModel *refModel, QObject *parent) :
	QObject(parent),
	m_moves(0),
	m_workModel(workModel),
	m_refModel(refModel),
	m_timer(new GameTimer(this))
{
	Q_ASSERT(m_workModel != nullptr);
	Q_ASSERT(m_refModel != nullptr);
	Q_ASSERT(m_workModel != m_refModel);

	connect(m_timer, SIGNAL(timeout(const QString &)), this, SIGNAL(timeChanged(const QString &)));
}

void GameProcessController::moveDice(const QModelIndex &index)
{
	if(m_workModel->moveElement(index))
	{
		emit movesChanged(++m_moves);

		if(!m_timer->isActive())
		{
			m_timer->start();
		}

		if(*m_workModel == *m_refModel)
		{
			m_timer->stop();
			emit gameOver();
		}
	}
}

void GameProcessController::newGame()
{
	//TODO: Сначала надо проверить запущена ли игра и предложить её продолжить...

	m_moves = 0;
	emit movesChanged(m_moves);

	m_timer->stop();
	m_timer->reset();

	shuffle();
}

/*
 * Перетасовать рабочую модель
 */
void GameProcessController::shuffle()
{
	m_workModel->shuffle();
}
