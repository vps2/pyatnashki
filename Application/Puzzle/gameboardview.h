#ifndef GAMEBOARDVIEW_H
#define GAMEBOARDVIEW_H

#include <QTableView>

class GameBoardView : public QTableView
{
		Q_OBJECT

	public:
		explicit GameBoardView(QWidget *parent = nullptr);
};

#endif // GAMEBOARDVIEW_H
