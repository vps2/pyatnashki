#include "mainform.h"
#include "constants.h"

#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QApplication::setApplicationName(Constants::APPLICATION_NAME);

	GameForm form;
	form.show();

	//открываем файл со стилями на чтение
	QFile file("./style/style.qss");
	file.open(QFile::ReadOnly);
	//считываем его в строку
	QString strCSS = QLatin1String(file.readAll());

	//применяем новый стиль к приложению
	qApp->setStyleSheet(strCSS);

	return app.exec();
}
