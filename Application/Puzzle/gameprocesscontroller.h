#ifndef GAMEPROCESSCONTROLLER_H
#define GAMEPROCESSCONTROLLER_H

#include <QObject>

class IGameBoardModel;
class QModelIndex;
class GameTimer;

class GameProcessController : public QObject
{
		Q_OBJECT

	public:
		GameProcessController(IGameBoardModel *workModel, IGameBoardModel *refModel,
									 QObject *parent = nullptr);

	public slots:
		void moveDice(const QModelIndex &index);
		void newGame();

	signals:
		void movesChanged(int);
		void timeChanged(const QString &);
		void gameOver();

	private:
		Q_DISABLE_COPY(GameProcessController)

		void shuffle();

		int m_moves;

		IGameBoardModel *m_workModel = nullptr;
		IGameBoardModel *m_refModel = nullptr;
		GameTimer *m_timer = nullptr;
};

#endif // GAMEPROCESSCONTROLLER_H
