#ifndef GAMETIMER_P_H
#define GAMETIMER_P_H

#include "gametimer.h"

#include <QTime>
#include <QTimer>

/*
 * Класс содержит закрытую реализацию методов, для класса GameTimer, т.е является
 * private классом в идиоме pimpl.
 */
class GameTimerPrivate
{
		Q_DECLARE_PUBLIC(GameTimer)	//макрос создаёт 2 inline метода "GameTimer* q_func()" и
		//const GameTimer* q_func(), которые решают проблему не константности указателя d_ptr,
		//а также объявляют класс GameTimer дружественным классу GameTimerPrivate.

	public:
		GameTimerPrivate();

		bool isActive() const;
		int getInterval() const;
		void setInterval(int msec);
		void start();
		void start(int msec);
		void stop();
		void reset();

		void _q_incrementTime();	//будет вызываться в качестве закрытого слота из класса GameTimer

		QTime m_time;
		QTimer m_timer;

		GameTimer *q_ptr = nullptr;	//указатель на экземпляр публичного класса
};

#endif // GAMETIMER_P_H
