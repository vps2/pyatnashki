TEMPLATE = app

greaterThan(QT_MAJOR_VERSION, 4) {
 CONFIG += c++14
 QT += widgets
 DEFINES += HAVE_QT5
} else {
 QMAKE_CXXFLAGS += -std=c++11
}

HEADERS += \
    celldelegate.h \
    mainform.h \
    gameboardmodel.h \
	 gametimer.h \
	 gametimer_p.h \
    gameboardview.h \
    constants.h \
    gameprocesscontroller.h \
    igameboardmodel.h \
	 iresultsmodule.h

SOURCES += \
    main.cpp \
    celldelegate.cpp \
    mainform.cpp \
    gameboardmodel.cpp \
    gametimer.cpp \
    gameboardview.cpp \
    gameprocesscontroller.cpp

TARGET = Pyatnashki
