#ifndef IRESULTSMODULE_H
#define IRESULTSMODULE_H

#include <QtPlugin>

class QWidget;

class IResultsModule
{
	public:
		virtual ~IResultsModule()
		{
		}

		virtual void setReferenceWidget(QWidget *refWidget = nullptr) = 0;
		virtual void showResults() = 0;
		virtual bool writeGameResult(const int moves, const QString &time) = 0;
};

Q_DECLARE_INTERFACE(IResultsModule, "ru.vps.Pyatnashki.IResultsModule/1.0")

#endif // IRESULTSMODULE_H
