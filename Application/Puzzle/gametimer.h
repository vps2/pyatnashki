#ifndef GAMETIMER_H
#define GAMETIMER_H

#include <QObject>

class QString;
class GameTimerPrivate;

class GameTimer : public QObject
{
		Q_OBJECT

	public:
		explicit GameTimer(QObject *parent = nullptr);
		~GameTimer() = default;	//обязательно должна быть реализация.

		bool isActive() const;
		int getInterval() const;
		void setInterval(int msec);

	public slots:
		void start();
		void start(int msec);
		void stop();
		void reset();
		
	signals:
		void timeout(const QString &);

	private:
		Q_DISABLE_COPY(GameTimer)	//вырубаем конструкторы копирования и оператор присваивания

		Q_DECLARE_PRIVATE_D(m_gameTimerPrivate.data(), GameTimer)	//макрос создаёт 2 inline метода "GameTimerPrivate* d_func()" и
		//const GameTimerPrivate* d_func(), которые решают проблему не константности указателя d_ptr,
		//а также объявляют класс GameTimerPrivate дружественным классу GameTimer.

		Q_PRIVATE_SLOT(d_func(), void _q_incrementTime())	//делает приватный метод класса GameTimerPrivate
		//доступным для вызова в качестве приватного слота из класса GameTimer

		QScopedPointer<GameTimerPrivate> m_gameTimerPrivate;
};

#endif // GAMETIMER_H
