#include "mainform.h"
#include "celldelegate.h"
#include "gameboardview.h"
#include "gameboardmodel.h"
#include "gameprocesscontroller.h"
#include "constants.h"
#include "iresultsmodule.h"

#include <QApplication>
#include <QAbstractButton>
#include <QDir>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QStatusBar>
#include <QSettings>
#include <QCloseEvent>
#include <QPluginLoader>

GameForm::GameForm(QWidget *parent) :
	QMainWindow(parent)
{
	loadPlugins();

	createModels();
	createWidgets();
	createActions();
	createMenu();
	createControllers();
	createConnections();

	positioningOfWidgets();

	statusBar()->setVisible(true);
	setMinimumSize(sizeHint());
	restoreSettings();

	emit newGame();

	retranslateUi();
}

void GameForm::createModels()
{
	m_refModel = new GameBoardModel(this);
	m_workModel = new GameBoardModel(this);
}

void GameForm::createWidgets()
{
	m_view = new GameBoardView();
	m_view->setObjectName("GameBoardView");
	m_view->setModel(m_workModel);
	m_view->setItemDelegate(new CellDelegate(m_view));

	m_statusBarMessagesLabel = new QLabel();
	m_statusBarMessagesLabel->setObjectName("statusBarMessagesLabel");

	m_timerLabel = new QLabel();
	m_timerLabel->setObjectName("timerLabel");
	m_timerLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);

	m_movesLabel = new QLabel();
	m_movesLabel->setObjectName("movesLabel");
	m_movesLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
}

void GameForm::createActions()
{
	m_newGameAction = new QAction(this);
	m_newGameAction->setShortcut(Qt::Key_F2);

	if(m_resultsModule)
	{
		m_showResultsAction = new QAction(this);
		m_showResultsAction->setShortcut(Qt::Key_F4);
	}

	m_exitAction = new QAction(this);

	m_aboutQtAction = new QAction(this);
	m_aboutQtAction->setShortcut(Qt::CTRL + Qt::Key_Q);

	m_aboutGameAction = new QAction(this);

}

void GameForm::createMenu()
{
	m_gameMenu = new QMenu(this);
	m_gameMenu->addAction(m_newGameAction);
	m_gameMenu->addSeparator();
	if(m_showResultsAction)
	{
		m_gameMenu->addAction(m_showResultsAction);
		m_gameMenu->addSeparator();
	}
	m_gameMenu->addAction(m_exitAction);

	m_languageMenu = createLanguageMenu();

	m_helpMenu = new QMenu(this);
	m_helpMenu->addAction(m_aboutQtAction);
	m_helpMenu->addAction(m_aboutGameAction);

	menuBar()->addMenu(m_gameMenu);
	menuBar()->addMenu(m_languageMenu);
	menuBar()->addMenu(m_helpMenu);
}

QMenu* GameForm::createLanguageMenu()
{
	QMenu *languageMenu = new QMenu(this);

	QActionGroup *actionsGroup = new QActionGroup(this);
	actionsGroup->setExclusive(true);
	connect(actionsGroup, SIGNAL(triggered(QAction*)), this, SLOT(languageChanged(QAction*)));

	QAction *defaultAction = createLanguageAction("English", "en");
	defaultAction->setChecked(true);
	actionsGroup->addAction(defaultAction);
	languageMenu->addAction(defaultAction);

	QDir folderOfTranslations = QString(Constants::LANGUAGE_FILES_FOLDER);
	QStringList files = folderOfTranslations.entryList(QStringList(Constants::TRANSLATION_FILE_PATTERN));
	for(QString fileName : files)
	{
		fileName.truncate(fileName.lastIndexOf("."));

		QTranslator translator;
		translator.load(fileName, Constants::LANGUAGE_FILES_FOLDER);
		QString language = translator.translate("GameForm", "English");
		QString locale = fileName.right(fileName.length() - (fileName.lastIndexOf("_") + 1));

		QAction *action = createLanguageAction(language, locale);
		actionsGroup->addAction(action);
		languageMenu->addAction(action);
	}

	return languageMenu;
}

QAction* GameForm::createLanguageAction(const QString &text, const QString &data)
{
	QAction *action = new QAction(text, this);
	action->setData(data);
	action->setCheckable(true);

	return action;
}

void GameForm::createControllers()
{
	m_gameProcessController = new GameProcessController(m_workModel, m_refModel, this);
}

void GameForm::createConnections()
{
	connect(this, SIGNAL(newGame()), m_gameProcessController, SLOT(newGame()));
	connect(m_gameProcessController, SIGNAL(gameOver()), this, SLOT(doPostGameOperations()));
	connect(m_view, SIGNAL(clicked(const QModelIndex &)),
			  m_gameProcessController, SLOT(moveDice(const QModelIndex &)));
	connect(m_gameProcessController, SIGNAL(movesChanged(int)),
			  this, SLOT(setMovesWidgetText(int)));
	connect(m_gameProcessController, SIGNAL(timeChanged(const QString &)),
			  this, SLOT(setTimeWidgetText(const QString &)));

	connect(m_newGameAction, SIGNAL(triggered(bool)), this, SIGNAL(newGame()));
	if(m_showResultsAction)
	{
		connect(m_showResultsAction, SIGNAL(triggered(bool)), this, SLOT(showResults()));
	}
	connect(m_exitAction, SIGNAL(triggered(bool)), this, SLOT(close()));
	connect(m_aboutQtAction, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt()));
	connect(m_aboutGameAction, SIGNAL(triggered(bool)), this, SLOT(about()));
}

void GameForm::positioningOfWidgets()
{
	setCentralWidget(m_view);

	statusBar()->addPermanentWidget(m_statusBarMessagesLabel, 1);
	statusBar()->addPermanentWidget(m_timerLabel, 1);
	statusBar()->addPermanentWidget(m_movesLabel, 1);
}

void GameForm::loadPlugins()
{
	QDir pluginsDir(Constants::PLUGINS_FOLDER);

	for(QString fileName : pluginsDir.entryList(QDir::Files))
	{
		try
		{
			QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
			m_resultsModule = qobject_cast<IResultsModule *>(loader.instance());

			if(m_resultsModule)
			{
				m_resultsModule->setReferenceWidget(this);
				break;
			}
		}
		catch(...)
		{
		}
	}
}

bool GameForm::okToNewGame()
{
	QScopedPointer<QMessageBox> messageBox(new QMessageBox(this));

	messageBox->setWindowModality(Qt::WindowModal);	//для правильного отображения в Mac OS X
	messageBox->setIcon(QMessageBox::Question);
	messageBox->setWindowTitle(tr("Game over"));
	messageBox->setText(tr("New game?"));
	QAbstractButton *newGameButton = messageBox->addButton(tr("&Yes"), QMessageBox::AcceptRole);
	messageBox->addButton(tr("&No"), QMessageBox::RejectRole);
	messageBox->setDefaultButton(qobject_cast<QPushButton *>(newGameButton));
	messageBox->exec();

	return messageBox->clickedButton() == newGameButton;
}

void GameForm::checkForNewGame()
{
	if(okToNewGame())
	{
		emit newGame();
	}
}

void GameForm::setMovesWidgetText(int moves)
{
	QString movesLabel = tr("Moves: %1").arg(moves);
	m_movesLabel->setText(movesLabel);
}

void GameForm::setTimeWidgetText(const QString &text)
{
	QString timeLabel = tr("Time: %1").arg(text);
	m_timerLabel->setText(timeLabel);
}

void GameForm::showResults()
{
	m_resultsModule->showResults();
}

void GameForm::about()
{
	QMessageBox::about(this, tr("About Game"),
							 tr("<center>The usual game of <b>15 Puzzle</b>.</center><br>"
								 "Was created with the aim of exploring "
								 "some of the technology framework Qt."));
}

void GameForm::restoreSettings()
{
	QSettings settings(Constants::SETTINGS_FILE, QSettings::IniFormat);

	restoreGeometry(settings.value(Constants::WINDOW_GEOMETRY).toByteArray());

	QString locale = settings.value(Constants::TRANSLATION, "en").toString();
	changeLanguage(locale);
	for(QAction *action : m_languageMenu->actions())
	{
		QString actionLocale = action->data().toString();
		if(actionLocale == locale)
		{
			action->setChecked(true);
			break;
		}
	}
}

void GameForm::saveSettings()
{
	QSettings settings(Constants::SETTINGS_FILE, QSettings::IniFormat);

	settings.setValue(Constants::WINDOW_GEOMETRY, saveGeometry());
	settings.setValue(Constants::TRANSLATION, m_currentLocale);
}

void GameForm::retranslateUi()
{
	QString title = m_translator.translate("main", Constants::APPLICATION_NAME);
	setWindowTitle(title.isEmpty() ? Constants::APPLICATION_NAME : title);

	m_gameMenu->setTitle(tr("&Game"));
	m_languageMenu->setTitle(tr("&Language"));
	m_helpMenu->setTitle(tr("&Help"));

	m_newGameAction->setText(tr("&New Game"));
	if(m_showResultsAction)
	{
		m_showResultsAction->setText(tr("&Results..."));
	}
	m_exitAction->setText(tr("E&xit"));
	m_aboutQtAction->setText(tr("About Qt"));
	m_aboutGameAction->setText(tr("About game"));

	QString movesLabelText = m_movesLabel->text();
	int moves = movesLabelText
					.right(movesLabelText.length() - (movesLabelText.lastIndexOf(" ") + 1))
					.toInt();
	setMovesWidgetText(moves);
	QString timeLabelText = m_timerLabel->text();
	setTimeWidgetText(timeLabelText.right(timeLabelText.length() - (timeLabelText.lastIndexOf(" ") + 1)));
}

void GameForm::changeLanguage(const QString &locale)
{
	if(!m_currentLocale.isEmpty() && m_currentLocale == locale)
	{
		return;
	}

	m_currentLocale = locale;
	qApp->removeTranslator(&m_translator);

	QString fileName = QString(Constants::TRANSLATION_FILE_PATTERN).replace("*", locale);
	if(m_translator.load(fileName, Constants::LANGUAGE_FILES_FOLDER))
	{
		qApp->installTranslator(&m_translator);
	}
}

void GameForm::languageChanged(QAction *action)
{
	changeLanguage(action->data().toString());
}

void GameForm::doPostGameOperations()
{
	if(m_resultsModule)
	{
		const int moves = m_movesLabel->text().toInt();
		const QString time = m_timerLabel->text();

		m_resultsModule->writeGameResult(moves, time);
	}

	checkForNewGame();
}

void GameForm::closeEvent(QCloseEvent *event)
{
	saveSettings();

	event->accept();
}

void GameForm::changeEvent(QEvent *event)
{
	if(event->type() == QEvent::LanguageChange)
	{
		retranslateUi();
	}

	QMainWindow::changeEvent(event);
}
