#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace Constants
{
	const char APPLICATION_NAME[] = QT_TRANSLATE_NOOP("main", "15 Puzzle");

	const char SETTINGS_FILE[] = "./settings/options.ini";
	const char LANGUAGE_FILES_FOLDER[] = "./langs";
	const char TRANSLATION_FILE_PATTERN[] = "puzzle_*.qm";

	const char PLUGINS_FOLDER[] = "./plugins";

	const char WINDOW_GEOMETRY[] = "main/geometry";
	const char TRANSLATION[] = "main/translation";
}

#endif // CONSTANTS_H
