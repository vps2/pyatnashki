#include "gametimer.h"
#include "gametimer_p.h"

#include <QString>

namespace
{
	const int DEFAULT_TIMEOUT = 1000;
}

GameTimerPrivate::GameTimerPrivate()
{
	m_time.setHMS(0,0,0,0);
	setInterval(DEFAULT_TIMEOUT);
}

bool GameTimerPrivate::isActive() const
{
	return m_timer.isActive();
}

int GameTimerPrivate::getInterval() const
{
	return m_timer.interval();
}

void GameTimerPrivate::setInterval(int msec)
{
	m_timer.setInterval(msec);
}

void GameTimerPrivate::start()
{
	int currInterval = getInterval();
	start(currInterval);
}

void GameTimerPrivate::start(int msec)
{
	if(isActive())
	{
		reset();
	}

	m_timer.start(msec);
}

void GameTimerPrivate::stop()
{
	m_timer.stop();
}

void GameTimerPrivate::reset()
{
	m_time.setHMS(0,0,0,0);

	Q_Q(GameTimer);
	emit q->timeout(m_time.toString("hh:mm:ss"));
}

void GameTimerPrivate::_q_incrementTime()
{
	int currInterval = getInterval();
	m_time = m_time.addMSecs(currInterval);
	
	Q_Q(GameTimer);	//возвращает указатель "q" на класс GameTimer для последующего вызова его методов
	emit q->timeout(m_time.toString("hh:mm:ss"));
}

//------------------------------------------------------------------------------

GameTimer::GameTimer(QObject *parent) :
	QObject(parent),
	m_gameTimerPrivate(new GameTimerPrivate())
{
	Q_D(GameTimer);	//возвращает указатель "d" на класс GameTimerPrivate для последующего
	//обращения к его полям и методам
	d->q_ptr = this;	//инициализируем собой указатель "q_ptr" приватного класса

	QObject::connect(&d->m_timer, SIGNAL(timeout()), this, SLOT(_q_incrementTime()));
}

bool GameTimer::isActive() const
{
	Q_D(const GameTimer);
	return d->isActive();
}

void GameTimer::start()
{
	Q_D(GameTimer);
	d->start();
}

void GameTimer::start(int msec)
{
	Q_D(GameTimer);
	d->start(msec);
}

void GameTimer::stop()
{
	Q_D(GameTimer);
	d->stop();
}

void GameTimer::reset()
{
	Q_D(GameTimer);
	d->reset();
}

int GameTimer::getInterval() const
{
	Q_D(const GameTimer);
	return d->getInterval();
}

void GameTimer::setInterval(int msec)
{
	Q_D(GameTimer);
	d->setInterval(msec);
}

#include "moc_gametimer.cpp"
