#include "gameboardview.h"

#include <QHeaderView>

GameBoardView::GameBoardView(QWidget *parent) :
	QTableView(parent)
{
	//инициализация внешнего вида и поведения представления
	horizontalHeader()->hide();
	verticalHeader()->hide();
#if HAVE_QT5
	horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
#else
	horizontalHeader()->setResizeMode(QHeaderView::Stretch);
	verticalHeader()->setResizeMode(QHeaderView::Stretch);
#endif
}
