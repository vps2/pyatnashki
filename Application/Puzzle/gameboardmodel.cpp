#include "gameboardmodel.h"

#include <QTime>

namespace
{
	const int DEFAULT_ROWS = 4;
	const int DEFAULT_COLUMNS = 4;
	const int NUMBER_STEPS_OF_MIXING = 500;
}

GameBoardModel::GameBoardModel(QObject *parent) :
	GameBoardModel(DEFAULT_ROWS, DEFAULT_COLUMNS, parent)
{
}

GameBoardModel::GameBoardModel(int rows, int columns, QObject *parent) :
	QAbstractTableModel(parent),
	m_numberOfRows(rows),
	m_numberOfColumns(columns)
{
	Q_ASSERT(m_numberOfRows > 0);
	Q_ASSERT(m_numberOfColumns > 0);

	m_storage.resize(m_numberOfRows);
	for(auto &item : m_storage)
	{
		item.resize(m_numberOfColumns);
	}

	qsrand(QTime(0,0,0).msecsTo(QTime::currentTime()));
	initialize();
}

QVariant GameBoardModel::data(const QModelIndex &index, int role) const
{
	if(index.isValid() && (role == Qt::DisplayRole))
	{
		int cellData = m_storage[index.row()][index.column()];
		if(cellData > 0)
		{
			return cellData;
		}
	}

	return QVariant();
}

bool GameBoardModel::setStorageData(const QModelIndex &index, const QVariant &value)
{
	if(!index.isValid())
	{
		return false;
	}

	m_storage[index.row()][index.column()] = value.value<int>();

	emit dataChanged(index, index);

	return true;
}

void GameBoardModel::initialize()
{
	int number = 1;
	int maxNumber = m_numberOfRows * m_numberOfColumns;

	//заполняем модель данными
	for(int row = 0; row < m_numberOfRows; ++row)
	{
		for(int col = 0; col < m_numberOfColumns; ++col)
		{
			QModelIndex &&cellIndex = index(row, col);

			if(number == maxNumber)
			{
				number = 0;
				m_emptyIndex = cellIndex;
			}

			setStorageData(cellIndex, number);

			++number;
		}
	}
}

bool GameBoardModel::moveElement(const QModelIndex &currCellIndex)
{
	if(currCellIndex.isValid())
	{
		QModelIndexList indexesList = getAdjacentIndexes(currCellIndex);

		if(indexesList.contains(m_emptyIndex))
		{
			int sourceCellValue = m_storage[currCellIndex.row()][currCellIndex.column()];
			int targetCellValue = m_storage[m_emptyIndex.row()][m_emptyIndex.column()];

			setStorageData(m_emptyIndex, sourceCellValue);
			setStorageData(currCellIndex, targetCellValue);

			m_emptyIndex = currCellIndex;

			return true;
		}
	}

	return false;
}

/*
 * Получить все соседние индексы текущего.
 */
QModelIndexList GameBoardModel::getAdjacentIndexes(const QModelIndex &currIndex) const
{	
	QModelIndexList indexesList;

	if(currIndex.isValid())
	{
		int newRowIndex = currIndex.row() - 1;
		if(newRowIndex >= 0)
		{
			QModelIndex &&cellIndex = index(newRowIndex, currIndex.column());
			indexesList.append(cellIndex);
		}
		newRowIndex = currIndex.row() + 1;
		if(newRowIndex < m_numberOfRows)
		{
			QModelIndex &&cellIndex = index(newRowIndex, currIndex.column());
			indexesList.append(cellIndex);
		}

		int newColIndex = currIndex.column() - 1;
		if(newColIndex >= 0)
		{
			QModelIndex &&cellIndex = index(currIndex.row(), newColIndex);
			indexesList.append(cellIndex);
		}
		newColIndex = currIndex.column() + 1;
		if(newColIndex < m_numberOfColumns)
		{
			QModelIndex &&cellIndex = index(currIndex.row(), newColIndex);
			indexesList.append(cellIndex);
		}
	}

	return indexesList;
}

int GameBoardModel::rowCount(const QModelIndex &index) const
{
	return index.isValid() ? 0 : m_numberOfRows;
}

int GameBoardModel::columnCount(const QModelIndex &index) const
{
	return index.isValid() ? 0 : m_numberOfColumns;
}

void GameBoardModel::shuffle()
{
	for(int i = 0; i < NUMBER_STEPS_OF_MIXING; ++i)
	{
		QModelIndexList indexesList = getAdjacentIndexes(m_emptyIndex);
		if(indexesList.isEmpty())
		{
			break;	//иначе на следующем этапе вывалится исключение деления на 0
		}

		const QModelIndex &index = indexesList.at(qrand() % indexesList.size());
		moveElement(index);
	}
}

bool GameBoardModel::operator ==(const IGameBoardModel &other) const
{
	const GameBoardModel &otherModel = static_cast<const GameBoardModel &>(other);
	return (m_storage == otherModel.m_storage);
}
