#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>
#include <QTranslator>

class GameBoardModel;
class GameProcessController;
class GameBoardView;
class QLabel;
class QMenu;
class QAction;
class IResultsModule;

class GameForm : public QMainWindow
{
		Q_OBJECT

	public:
		explicit GameForm(QWidget *parent = nullptr);

	signals:
		void newGame();

	protected:
		virtual void closeEvent(QCloseEvent *event) override;
		virtual void changeEvent(QEvent *event) override;

	private:
		void createModels();
		void createControllers();
		void createWidgets();
		void createActions();
		void createMenu();
		QMenu* createLanguageMenu();
		QAction* createLanguageAction(const QString &text, const QString &data);
		void createConnections();
		void positioningOfWidgets();

		void loadPlugins();

		void checkForNewGame();
		bool okToNewGame();
		void restoreSettings();
		void saveSettings();
		void retranslateUi();
		void changeLanguage(const QString &locale);

	private slots:
		void languageChanged(QAction *action);
		void doPostGameOperations();
		void setMovesWidgetText(int moves);
		void setTimeWidgetText(const QString &text);
		void showResults();
		void about();

	private:
		QTranslator m_translator;
		QString m_currentLocale;
		//
		GameBoardModel *m_refModel = nullptr;
		GameBoardModel *m_workModel = nullptr;
		GameProcessController *m_gameProcessController = nullptr;
		GameBoardView *m_view = nullptr;
		QLabel *m_statusBarMessagesLabel = nullptr;
		QLabel *m_timerLabel = nullptr;
		QLabel *m_movesLabel = nullptr;
		QMenu *m_gameMenu = nullptr;
		QMenu *m_languageMenu = nullptr;
		QMenu *m_helpMenu = nullptr;
		QAction *m_newGameAction = nullptr;
		QAction *m_showResultsAction = nullptr;
		QAction *m_exitAction = nullptr;
		QAction *m_aboutQtAction = nullptr;
		QAction *m_aboutGameAction = nullptr;

		IResultsModule *m_resultsModule = nullptr;
};

#endif // MAINFORM_H
