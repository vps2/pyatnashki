#ifndef IGAMEBOARDMODEL_H
#define IGAMEBOARDMODEL_H

class QModelIndex;

class IGameBoardModel
{
	public:
		virtual ~IGameBoardModel()
		{
		}

		virtual bool moveElement(const QModelIndex &currCellIndex) = 0;
		virtual void shuffle() = 0;	
      virtual bool operator==(const IGameBoardModel &other) const = 0;
};

#endif // IGAMEBOARDMODEL_H
