#include "celldelegate.h"

CellDelegate::CellDelegate(QObject *parent) :
	QStyledItemDelegate(parent)
{
}

void CellDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
								 const QModelIndex &index) const
{
	QVariant indexData(index.data(Qt::DisplayRole));
	if(!indexData.isValid())
	{
		return;
	}

	QStyleOptionViewItemV4 opt(option);
	initStyleOption(&opt, index);
	opt.displayAlignment = Qt::AlignCenter;

	QStyledItemDelegate::paint(painter, opt, index);
}
