#include "autotest/auto_test.h"

#include "puzzle/gameprocesscontroller.h"
#include "puzzle/gameboardmodel.h"

#include <QSignalSpy>

/*
 * Вид модели после создания:
 *
 * 1   2   3   4
 * 5   6   7   8
 * 9   10  11  12
 * 13  14  15  0
 *
 */

class tst_GameProcessController : public QObject
{
		Q_OBJECT

	private Q_SLOTS:
		void initTestCase();
		void cleanupTestCase();
		void init();
		void cleanup();

		void moveDiceWithIndex2x3WillIncreaseNumberOfMoves();
		void tryMoveDiceWithIndex2x2WillNotIncreaseNumberOfMoves();
		void whenNewGameNumberOfMovesIsSetToZero();

	private:
		void moveDiceWithIndex(int row, int column);

		GameProcessController *gameProcessController;
		IGameBoardModel *refModel;
		IGameBoardModel *workModel;

};

void tst_GameProcessController::initTestCase()
{
	refModel = new GameBoardModel();
}

void tst_GameProcessController::cleanupTestCase()
{
	delete refModel;
}

void tst_GameProcessController::init()
{
	workModel = new GameBoardModel();
	gameProcessController = new GameProcessController(workModel, refModel);
}

void tst_GameProcessController::cleanup()
{
	delete gameProcessController;
	delete workModel;
}

void tst_GameProcessController::moveDiceWithIndex2x3WillIncreaseNumberOfMoves()
{
	QSignalSpy spy(gameProcessController, SIGNAL(movesChanged(int)));

	moveDiceWithIndex(2, 3);

	int moves = spy.takeFirst().at(0).toInt();

	QCOMPARE(moves, 1);
}

void tst_GameProcessController::tryMoveDiceWithIndex2x2WillNotIncreaseNumberOfMoves()
{
	QSignalSpy spy(gameProcessController, SIGNAL(movesChanged(int)));

	moveDiceWithIndex(2, 2);

	QCOMPARE(spy.count(), 0);
}

void tst_GameProcessController::whenNewGameNumberOfMovesIsSetToZero()
{
	QSignalSpy spy(gameProcessController, SIGNAL(movesChanged(int)));

	moveDiceWithIndex(2, 3);
	gameProcessController->newGame();

	int moves = spy.at(1).at(0).toInt();

	QCOMPARE(moves, 0);
}

void tst_GameProcessController::moveDiceWithIndex(int row, int column)
{
	QModelIndex index = (static_cast<GameBoardModel *>(workModel))->index(row, column);
	gameProcessController->moveDice(index);
}


DECLARE_TEST(tst_GameProcessController)

#include "tst_gameprocesscontroller.moc"
