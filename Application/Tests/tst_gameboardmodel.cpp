#include "autotest/auto_test.h"

#include "puzzle/gameboardmodel.h"

#include <QSignalSpy>
#include <QVariant>

#include <memory>

/*
 * Вид модели после создания:
 *
 * 1   2   3   4
 * 5   6   7   8
 * 9   10  11  12
 * 13  14  15  0
 *
 */

class tst_GameBoardModel : public QObject
{
		Q_OBJECT

	private Q_SLOTS:
		void initTestCase();
		void cleanupTestCase();
		void init();
		void cleanup();

		void defaultModelHas4RowsAnd4Columns();
		void createModelWith3RowsAnd4Columns();
		void twoModelsAfterCreatingIsEquals();
		void modelsAfterShuffleIsNotEquals();
		void indexWithRow2AndColumn2IsNotMoveable();
		void indexWithRow2AndColumn3Ismoveable();
		void modelEmitsDataChangedSignalAfterMoveDice();

	private:
		GameBoardModel *defaultModel;
};

void tst_GameBoardModel::initTestCase()
{
}

void tst_GameBoardModel::cleanupTestCase()
{
}

void tst_GameBoardModel::init()
{
	defaultModel = new GameBoardModel();
}

void tst_GameBoardModel::cleanup()
{
	delete defaultModel;
}

void tst_GameBoardModel::defaultModelHas4RowsAnd4Columns()
{
	QCOMPARE(defaultModel->rowCount(QModelIndex()), 4);
	QCOMPARE(defaultModel->columnCount(QModelIndex()), 4);
}

void tst_GameBoardModel::createModelWith3RowsAnd4Columns()
{
	GameBoardModel model(3, 4);

	QCOMPARE(model.rowCount(QModelIndex()), 3);
	QCOMPARE(model.columnCount(QModelIndex()), 4);
}

void tst_GameBoardModel::twoModelsAfterCreatingIsEquals()
{
	std::unique_ptr<GameBoardModel> anotherModel(new GameBoardModel());

	QVERIFY(*defaultModel == *anotherModel);
}

void tst_GameBoardModel::modelsAfterShuffleIsNotEquals()
{
	std::unique_ptr<GameBoardModel> anotherModel(new GameBoardModel());

	defaultModel->shuffle();
	anotherModel->shuffle();

	QCOMPARE(*defaultModel == *anotherModel, false);
}

void tst_GameBoardModel::indexWithRow2AndColumn2IsNotMoveable()
{
	QModelIndex index = defaultModel->index(2, 2);

	QCOMPARE(defaultModel->moveElement(index), false);
}

void tst_GameBoardModel::indexWithRow2AndColumn3Ismoveable()
{
	QModelIndex index = defaultModel->index(2, 3);

	QCOMPARE(defaultModel->moveElement(index), true);
}

//TODO: тест не совсем правильный, т.к. хотелось бы проверить значение индексов
void tst_GameBoardModel::modelEmitsDataChangedSignalAfterMoveDice()
{
	qRegisterMetaType<QModelIndex>("QModelIndex");

	QSignalSpy spy(defaultModel, SIGNAL(dataChanged(QModelIndex, QModelIndex)));
	QModelIndex index = defaultModel->index(2, 3);

	defaultModel->moveElement(index);

//	данные перемещаются в 2 этапа
	QCOMPARE(spy.count(), 2);
}

DECLARE_TEST(tst_GameBoardModel)

#include "tst_gameboardmodel.moc"
