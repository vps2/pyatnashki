greaterThan(QT_MAJOR_VERSION, 4) {
 CONFIG += c++14
} else {
 QMAKE_CXXFLAGS += -std=c++11
}

QT += testlib

QT -= gui

TARGET = tests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

DEFINES += SRCDIR=\\\"$$PWD/\\\"

include(../appdefault.pri)

HEADERS += \ 
	$$APP_DIR/gameboardmodel.h \
	$$APP_DIR/gameprocesscontroller.h \
	$$APP_DIR/gametimer.h

SOURCES += \
	tests_main.cpp \
	tst_gameboardmodel.cpp \
	tst_gameprocesscontroller.cpp \
	$$APP_DIR/gameboardmodel.cpp \
	$$APP_DIR/gameprocesscontroller.cpp \
	$$APP_DIR/gametimer.cpp
