#include "resultsmoduleplugin.h"
#include "ui_resultsform.h"

#include <QTime>

ResultsModulePlugin::ResultsModulePlugin() :
	m_resultsForm(new Ui::ResultsDialog())
{
	m_resultsForm->setupUi(this);

	//FIXME: сделано для имитация записи результатов игры в хранилище. Потом надо будет убрать!!!
	QTime time = QTime::currentTime();
	qsrand(static_cast<uint>(time.msec()));
}

ResultsModulePlugin::~ResultsModulePlugin()
{
}

void ResultsModulePlugin::setReferenceWidget(QWidget *refWidget)
{
	m_referenceWidget = refWidget;
}

void ResultsModulePlugin::showResults()
{
	if(m_referenceWidget)
	{
		setInCenter(m_referenceWidget);
	}

	exec();
}

bool ResultsModulePlugin::writeGameResult(const int moves, const QString &time)
{
	//FIXME: имитация записи результатов игры в хранилище. Надо переделать!!!
	Q_UNUSED(moves)
	Q_UNUSED(time)

	bool result = qrand() % 2;

	if(result)
	{
		showResults();	//типа данные надо записать в результирующую таблицу
	}

	return result;
}

void ResultsModulePlugin::changeEvent(QEvent *event)
{
	if(event->type() == QEvent::LanguageChange)
	{
		m_resultsForm->retranslateUi(this);
	}
}

void ResultsModulePlugin::setInCenter(const QWidget *refWidget)
{
	if(!refWidget)
	{
		return;
	}

	QPoint center((refWidget->width() / 2) + refWidget->pos().x(),
					  (refWidget->height() / 2) + refWidget->pos().y());
	QPoint topLeft(center.x() - (width() / 2),
						center.y() - (height() / 2));

	move(topLeft);

}

#ifndef HAVE_QT5
Q_EXPORT_PLUGIN2(results_module, ResultsModulePlugin)
#endif
