TEMPLATE = lib

CONFIG += plugin

greaterThan(QT_MAJOR_VERSION, 4) {
 CONFIG += c++14
 QT += widgets
 DEFINES += HAVE_QT5
} else {
 QMAKE_CXXFLAGS += -std=c++11
}

TARGET_APP_PATH = ../application/puzzle

INCLUDEPATH += $${TARGET_APP_PATH}

HEADERS += \
	 $${TARGET_APP_PATH}/iresultsmodule.h \
    resultsmoduleplugin.h

SOURCES += \
    resultsmoduleplugin.cpp

TARGET = $$qtLibraryTarget(resultsmodule)

DESTDIR += $${TARGET_APP_PATH}/plugins

FORMS += \
    resultsform.ui


