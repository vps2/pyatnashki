/********************************************************************************
** Form generated from reading UI file 'resultsform.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESULTSFORM_H
#define UI_RESULTSFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>

QT_BEGIN_NAMESPACE

class Ui_ResultsDialog
{
public:
    QGridLayout *gridLayout;
    QTableView *tableView;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnClose;

    void setupUi(QDialog *ResultsDialog)
    {
        if (ResultsDialog->objectName().isEmpty())
            ResultsDialog->setObjectName(QStringLiteral("ResultsDialog"));
        ResultsDialog->resize(338, 350);
        ResultsDialog->setModal(false);
        gridLayout = new QGridLayout(ResultsDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tableView = new QTableView(ResultsDialog);
        tableView->setObjectName(QStringLiteral("tableView"));

        gridLayout->addWidget(tableView, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnClose = new QPushButton(ResultsDialog);
        btnClose->setObjectName(QStringLiteral("btnClose"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btnClose->sizePolicy().hasHeightForWidth());
        btnClose->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(btnClose);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);


        retranslateUi(ResultsDialog);
        QObject::connect(btnClose, SIGNAL(clicked()), ResultsDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(ResultsDialog);
    } // setupUi

    void retranslateUi(QDialog *ResultsDialog)
    {
        ResultsDialog->setWindowTitle(QApplication::translate("ResultsDialog", "Results Table", 0));
        btnClose->setText(QApplication::translate("ResultsDialog", "&Close", 0));
    } // retranslateUi

};

namespace Ui {
    class ResultsDialog: public Ui_ResultsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESULTSFORM_H
