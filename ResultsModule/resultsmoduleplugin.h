#ifndef RESULTSMODULEPLUGIN_H
#define RESULTSMODULEPLUGIN_H

#include "iresultsmodule.h"

#if HAVE_QT5
#include <QtWidgets/QDialog>
#else
#include <QDialog>
#endif

namespace Ui
{
	class ResultsDialog;
}

class ResultsModulePlugin : public QDialog, public IResultsModule
{
		Q_OBJECT
#ifdef HAVE_QT5
		Q_PLUGIN_METADATA(IID "ru.vps.Pyatnashki.IResultsModule/1.0" FILE "results_module_plugin.json")
#endif
		Q_INTERFACES(IResultsModule)

	public:
		ResultsModulePlugin();
		~ResultsModulePlugin();

		void setReferenceWidget(QWidget *refWidget) override;
		void showResults() override;
		bool writeGameResult(const int moves, const QString &time) override;

	protected:
		virtual void changeEvent(QEvent *event) override;

	private:
		void setInCenter(const QWidget *refWidget);

		QScopedPointer<Ui::ResultsDialog> m_resultsForm;
		//
		QWidget *m_referenceWidget = nullptr;
};

#endif // RESULTSMODULEPLUGIN_H
