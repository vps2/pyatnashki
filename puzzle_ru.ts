<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>GameForm</name>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="292"/>
        <source>&amp;Game</source>
        <translation>&amp;Игра</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="293"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="296"/>
        <source>&amp;New Game</source>
        <translation>&amp;Новая игра</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="301"/>
        <source>E&amp;xit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="294"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="299"/>
        <source>&amp;Results...</source>
        <translation>&amp;Таблица результатов...</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="302"/>
        <source>About Qt</source>
        <oldsource>&amp;About Qt</oldsource>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="303"/>
        <source>About game</source>
        <translation>О игре</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="218"/>
        <source>Game over</source>
        <translation>Игра закончена</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="134"/>
        <source>English</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="219"/>
        <source>New game?</source>
        <translation>Начать новую игру?</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="220"/>
        <source>&amp;Yes</source>
        <translation>&amp;Да</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="221"/>
        <source>&amp;No</source>
        <translation>&amp;Нет</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="238"/>
        <source>Moves: %1</source>
        <translation>Число ходов: %1</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="244"/>
        <source>Time: %1</source>
        <translation>Время: %1</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="255"/>
        <source>About Game</source>
        <translation>Об игре</translation>
    </message>
    <message>
        <location filename="application/puzzle/mainform.cpp" line="256"/>
        <source>&lt;center&gt;The usual game of &lt;b&gt;15 Puzzle&lt;/b&gt;.&lt;/center&gt;&lt;br&gt;Was created with the aim of exploring some of the technology framework Qt.</source>
        <translation>&lt;center&gt;Обычная игра - &lt;b&gt;Пятнашки&lt;/b&gt;.&lt;/center&gt;&lt;br&gt;Создавалась с целью изучения некоторых технологий фреймворка Qt.</translation>
    </message>
</context>
<context>
    <name>ResultsDialog</name>
    <message>
        <location filename="resultsmodule/resultsform.ui" line="14"/>
        <source>Results Table</source>
        <translation>Таблица результатов</translation>
    </message>
    <message>
        <location filename="resultsmodule/resultsform.ui" line="50"/>
        <source>&amp;Close</source>
        <oldsource>Close</oldsource>
        <translation>&amp;Закрыть</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="application/puzzle/constants.h" line="6"/>
        <source>15 Puzzle</source>
        <translation>Пятнашки</translation>
    </message>
</context>
</TS>
